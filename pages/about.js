import React from "react";
import Container from "../components/Container";

const About = () => {
  return (
    <Container titulo="Sobre">
      <h1>Sobre</h1>
    </Container>
  );
};

export default About;
